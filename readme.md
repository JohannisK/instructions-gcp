# Google Cloud Platform 1 - CI/CD

## Before you start

Ga naar https://console.cloud.google.com en maak een account aan. 
Je kunt mogelijk je bestaande gmail/jcore account gebruiken.

Maak een project aan en geef het een herkenbare naam.  
Bijvoorbeeld: JCore CICD

Zorg ook dat dit project bovenin is geselecteerd.

We gaan straks verschillende features van Google Cloud bekijken, zorg voor nu dat in ieder geval Compute Engine beschikbaar is.

Ga in het hamburger menu naar `Compute Engine`. Als de API nog niet beschikbaar is klik je op `Enable API`

## Source Repositories

Je kunt, door je omgevingen juist in te richten, iedere Git repository gebruiken om je code te versionen.  
Google bied Source Repositories aan. Je kunt Source Repositories koppelen aan een andere git repository. 
Voor deze tutorial maak je gebruik maken van de eigen git repositories van google in Source Repositories.

Ga in het hamburger menu naar `Source Repositories`  
![Source Repositories](https://storage.googleapis.com/jcore-cicd-images/GCP1-1.png)

1. Voeg een nieuwe repository toe, selecteer `Push code from a local repository`  
![Add a repository](https://storage.googleapis.com/jcore-cicd-images/GCP1-2.png "Add a repository")
2. Als je dit nog niet eerder gedaan hebt, moet je de stappen doorlopen om een ssh key toe te voegen.
3. Maak een nieuw Spring Boot 2.7.8 project aan met gradle. Zet de sourcecompatibility op java 11. Voeg daar de dependency `Spring Web Starter` aan toe. (Je kunt natuurlijk gebruik maken van https://start.spring.io/)
4. Voeg een Controller toe (zie onderstaand voorbeeld). Je hebt nu een java project om te gaan builden in GCP
5. Voer in de directory `git init` uit. Voeg vervolgens de google remote toe via de url die google toont.  
    **Let Op!** Google adviseert `git remote add google`. Daarmee noem je je origin google. Je kunt de origin ook gewoon origin noemen door het commando aan te passen naar `git remote add origin ...`
6. Commit en push de code.  
    **Let Op!** Google gebruikt `master` als hoofdbranch, terwijl Git zelf main adviseert. Push dus naar `master`.

```java
    import org.springframework.web.bind.annotation.GetMapping;
    import org.springframework.web.bind.annotation.RestController;
 
    @RestController
    public class HelloWorld {
 
        @GetMapping("/")
        public String get() {
            return "Hello World!"; 
        }
    }
```

## IAM

In google cloud zit een uitgebreid systeem om rechten en rollen toe te kennen aan gebruikers. 
Voor de buildserver(s) ga je een service account aanmaken die een aantal rechten en rollen heeft die passen bij een buildserver.

Ga in het hamburger menu naar `IAM & admin` -> `Service accounts`  
![Service accounts](https://storage.googleapis.com/jcore-cicd-images/GCP1-3.png)

1. Voeg een nieuwe Service account toe  
![Account Configuration](https://storage.googleapis.com/jcore-cicd-images/GCP1-4.png)
2. Ken de volgende rechten toe:
    - Compute Instance Admin (v1) _(Deze gebruiker mag VM instances maken)_
    - Service Account User _(Deze gebruiker is een Service Account)_
    - Source Repository Reader _(Deze gebruiker mag Source Repositories lezen)_
    - Storage Admin _(Deze gebruiker mag Storage Buckets beheren)_
3. Creëer ook een JSON Key en download deze

## Compute Engine 1
#### Jenkins Build Slave Image

De jenkins installatie gaat straks bestaan uit een Jenkins master en Jenkins Build slaves. Je gaat nu een build slave inrichten.

Ga in het hamburger menu naar `Compute Engine` -> `VM instances`  
![VM Instanes](https://storage.googleapis.com/jcore-cicd-images/GCP1-5.png)

1. Voeg een nieuwe VM instance toe
2. Pas de volgende waardes aan:
    - Name: jenkins-buildslave-template
    - Region: europe-west1
    - Machine type: f1-micro (Onder series N1)
    ![VM Configuration](https://storage.googleapis.com/jcore-cicd-images/GCP1-6.png)
3. Klik op `Change` onder Boot disk
4. Pas de volgende waardes aan:
    - OS: Ubuntu 18.04 LTS
    - Deletion rule: Keep boot disk
    ![Disks Configuration](https://storage.googleapis.com/jcore-cicd-images/GCP1-7.png)
5. Klik op create en wacht tot de VM is opgestart. Klik dan op SSH
6. Install java:  
`sudo apt-get update`  
`sudo apt-get install -y default-jdk`  
`java --version`
7. Sluit het venster en verwijder de VM instance
8. Ga in het hamburger menu naar `Compute Engine` -> `Disks` en verifieer dat de bootdisk nog bestaat
9. Ga in het hamburger menu naar `Compute Engine` -> `Images` en voeg een disk image toe  
    ![Disk Images](https://storage.googleapis.com/jcore-cicd-images/GCP1-8.png)
10. Geef de image een naam, en zorg dat de bron de bootdisk van de build-slave instance is.  
    ![Images Configuration](https://storage.googleapis.com/jcore-cicd-images/GCP1-8.png)
11. Klik create.

## Storage Bucket

Jenkins gaat de gebouwde artifacts en build logs opslaan in een GCP Storage Bucket.

Ga in het hamburger menu naar `Cloud Storage`  
![Storage](https://storage.googleapis.com/jcore-cicd-images/GCP1-10.png)

1. Voeg een nieuwe Bucket toe
2. Geef de bucket een naam. Laat alle andere settings staan.

## Jenkins installatie

De Jenkins Master gaat draaien op een normale VM. Je kunt een kale VM opspinnen en daarop Jenkins installeren.
Google bied echter ook een marketplace met voorgeconfigureerde solutions.

Ga in het hamburger menu naar `Marketplace`  
![Marketplace](https://storage.googleapis.com/jcore-cicd-images/GCP1-11.png)

1. Zoek naar `Jenkins Certified by Bitnami`
2. Klik op de solution
3. Klik op Launch on compute engine
4. Je kunt de VM hier nog customizen, maar de defaults zijn voor de use-case prima. Zorg wel dat je deployed in region europe-west1-b. Klik op deploy.
5. Ga naar het webadres van de Jenkins Master
6. Log in met de gegevens uit Deployment Manager.
7. Log in en klik op `Manage Jenkins` en daarna op `Manage Plugins`
8. Ga naar `Available` en installeer de volgende plugins:
    - Google Compute Engine plugin
    - Google Cloud Storage plugin
    - Google Authenticated Source plugin
    - Git plugin
9. Klik download now and install after restart
10. Klik de `Restart Jenkins when installation is complete and no jobs are running` checkbox

## Jenkins configuratie

Als de installatie is afgerond kun je weer inloggen in Jenkins. Jenkins moet nu gekoppeld worden aan GCP.

1. Ga naar `Manage Jenkins` -> `Global Tools Configuration`.
3. Ga naar `Manage Jenkins` -> `Manage Credentials` -> `System` en klik op `Global Credentials`
4. Voeg credentials toe:
    - Kind: Google Service Account from private key
    - Project Name: \<de project name die je in GCP gebruikt>
    - JSON key: \<upload de json key die je eerder gedownload hebt>
5. Gan naar `Manage Jenkins` -> `Manage Nodes and Clouds` -> `Configure Clouds` en klik helemaal onderaan op `Add a new Cloud`
6. Voeg een Google Compute Engine toe
    - Name: GCP
    - Project Name: \<de project name die je in GCP gebruikt>
    - Instance Cap: 4
    - Service Account Credentials: \<Selecteer de credentials die je eerder hebt toegevoegd>
7. Voeg nu een Instance Configuration toe:
    - Pas General aan:
        - Name Prefix: jenkins-slave
        - Description: Build slave voor Jenkins
        - Node Retention Time: 6
        - Usage: Use this node as much as possible
        - Labels gce-slave
        - Number of Executors: 1
    - Pas Location aan:
        - Region: europe-west1
        - Zone: europe-west1-b
8. Pas ook advanced configuration aan:
    - Pas Machine Configuration aan:
        - Machine Type: n1-standard-1
        - Preemtible?: true
    - Pas Networking aan:
        - General: Available networks
        - Network: default
        - Subnetwork: default
        - Attach External IP?: true
    - Pas Boot Disk aan:
        - Image project: \<de project name die je in GCP gebruikt>
        - Image name: \<de naam die je aan de jenkins image hebt gegeven>
        - Disk Type: pd-ssd
        - Size: 10
        - Delete on termination?: true
9. Voeg nu een job toe aan Jenkins. Noem de job `test` en selecteer een free style project
10. Pas General aan:
    - Execute concurrent builds if necessary: true
    - Restrict where this project can be run: true
    - Label Expression: gce-slave
11. Voeg bij Build 2 build steps toe:
    -  Execute shell:
        - Command: echo "Hello World!"
    -  Execute shell:
        - Command: java --version
12. Voer de job uit. Als het goed is zou na enige tijd een Jenkins slave moeten draaien waar de build correct op wordt uitgevoerd.
13. Voer de job nogmaal uit. Als het goed is gaat de build nu veel sneller omdat Jenkins de bestaande slave kan gebruiken.
14. Delete het project uit Jenkins.

## Jenkins job configuratie

Als laatste moet er een Jenkins job gekoppeld worden aan ons project.

1. Voeg een nieuwe Job toe. Kies weer voor free-style project
2. Pas General aan:
    - Execute concurrent builds if necessary: true
    - Restrict where this project can be run: true
    - Label Expression: gce-slave
3. Pas Source Code Management aan:
    - Selecteer Git:
        - Repository URL: \<De url van de git repository in Google Source Repositories>
        - Credentials: \<Selecteer de service account credentials>
        - Branch Specifier: */master
4. Pas Build Triggers aan:
    - Poll SCM: true
    - Schedule: * * * * *
5. Pas Build aan:
    - Execute shell:
        - Command: ./gradlew build
6. Pas Post-build Actions aan:
    - Voeg de Google Cloud Storage Plugin toe
        - Google Credentials: \<Selecteer de GCP credentials>
        - Voeg een Build Log Upload toe:
            - Log Name: build-log.txt
            - Storage Location: gs://\<De bucket url>/\$JOB_NAME/\$BUILD_NUMBER
        - Voeg een Classic Upload toe:
            - File Pattern: build/libs/*.jar
            - Storage Location: gs://\<De bucket url>/\$JOB_NAME/\$BUILD_NUMBER
            - Strip path prefix?: true
            - Path prefix: build/libs/
7. Doe een change in de git repository en push deze. De build zou automatisch moeten starten. Controlleer ook de storage bucket. De artifact en logs moeten geupload zijn.

## LifeCycle Rules

In de Storage module zitten een aantal opties om de data opslag goedkoper te maken. Bestanden die direct beschikbaar moeten zijn kosten het meest. 
Bestanden die niet direct beschikbaar hoeven te zijn kunnen worden opgeslagen als Nearline (snel beschikbaar) en Coldline (niet snel beschikbaar)

Om kosten te besparen kun je automatisch bestanden laten verplaatsen, onder bepaalde voorwaarden.

Ga in het hamburger menu naar `Cloud Storage`  
![Storage](https://storage.googleapis.com/jcore-cicd-images/GCP1-10.png)

1. Selecteer de bucket waar de artifacts in geplaatst worden.
2. Klik op `LIFECYCLE` en voeg een Lifecycle rule toe
3. Configureer de rule zodat deze bestanden
    - ouder dan 30 dagen
    - met een Storage class Regional, Standard of Durable Reduced Availability  
    naar Nearline zet.
4. Voeg nog een regel toe
5. Configureer de rule zodat deze bestanden
    - ouder dan 365 dagen
    - met een Storage class Nearline  
    naar Coldline zet.
    
## SonarQube

Het is ook mogelijk om custom images toe te voegen aan GCP. SonarQube heeft wat extra configuratie in GCP nodig,
In het volgende stuk wordt een installatie van een custom image met SonarQube behandeld.

#### SonarQube VM
1. Download het image voor SonarQube (https://storage.googleapis.com/jcore-cicd-images/sonarqube.tar.gz)
2. Maak een storage bucket aan en upload het image (zie eventueel het kopje Storage aan het begin voor extra instructie)
3. Ga in het hamburger menu naar `Compute Engine` -> `Images`  
   ![Images](https://storage.googleapis.com/jcore-cicd-images/GCP1-12.png)
4. Voeg een image toe op basis van de Cloud Storage file  
   ![Image config](https://storage.googleapis.com/jcore-cicd-images/GCP1-13.png)
5. Maak een standaard vm op basis van de image (zie eventueel het kopje Jenkins Slave voor instructie)

#### SonarQube DB
Ga in het hamburger menu naar `SQL`  
![SQL](https://storage.googleapis.com/jcore-cicd-images/GCP1-14.png)

1. Voeg een Mysql database toe, en vervang het gegenereerde wachtwoord door een eigen wachtwoord.  
   ![SQL config](https://storage.googleapis.com/jcore-cicd-images/GCP1-15.png)
2. Klik ook op `Show configuration options` en zorg dat alleen Private IP geselecteerd is.
3. Doorloop de stappen voor private access, gebruik de automatische configuraties.
4. Noteer (na het aanmaken) het private ip van de instantie
5. Log via ssh in op de sonarqube vm
6. Voer `sudo apt update` en `sudo apt upgrade` uit. (Mogelijk moet je de lockfiles verwijderen... foutje in de vm: `sudo rm /var/lib/dpkg/lock-frontend && sudo rm /var/lib/dpkg/lock`)
7. Ga naar `/opt/sonarqube/conf` en edit `sonar.properties` 
   - sonar.jdbc.username=postgres
   - sonar.jdbc.password=\<password>
   - sonar.jdbc.url=jdbc:postgresql://\<private ip>/postgres
8. Voer het commando `sudo systemctl restart sonar` uit
9. Geef Sonar echt een kwartier om op te starten. Dit duurt helaas even. Je kunt ondertussen gewoon verder :)

#### SonarQube Port
Ga in het hamburger menu naar `VPC network` -> `Firewall`  
![Firewall](https://storage.googleapis.com/jcore-cicd-images/GCP1-16.png)

1. Voeg een nieuwe firewall rule toe  
   ![Firewall config](https://storage.googleapis.com/jcore-cicd-images/GCP1-17.png)
2. Pas Target tags aan, vul hier `sonar` in.
3. Allow tcp op poort 9000, van source ips 0.0.0.0
4. Ga nu naar de VM instances en edit de SonarQube VM. Voeg bij network tags `sonar` toe.

Het kan even duren voordat SonarQube helemaal geconfigureerd is, maar deze zou beschikbaar moeten komen op het publieke ip, op poort 9000.

#### SonarQube Test
Als laatst gaat gradle een test uitvoeren tegen de SonarQube server.

1. Maak in SonarQube een nieuw project aan en configureer het.
2. Voeg aan de jenins de volgende `execute shell` buildstep toe:  
   `./gradlew sonarqube -Dsonar.projectKey=<naam van sonar project> -Dsonar.host.url=http://<intern ip van sonar vm>:9000 -Dsonar.login=<key uit sonar>`
3. Voeg aan het spring boot project de volgende plugin toe in build.gradle: ` id "org.sonarqube" version "2.7"`
4. Push de changes. Na een build in Jenkins geeft SonarQube het rapport weer

## Google Cloud Build

De Jenkins kan nu artifacts bouwen en verieferen tegen de sonar server. Google bied echter zelf ook build tooling aan, die vrij makkelijk intergeert met GCP.

### Disable Jenkens, enable Google Cloud Build

Zet de jenkins en sonar server uit in het VM instances menu
Ga in het hamburger menu naar `APIs & Services`  
![APIs & Services](https://storage.googleapis.com/jcore-cicd-images/GCP1-18.png)

1. Zoek naar `appengine` en enable de drie API's
![Enable appengine](https://storage.googleapis.com/jcore-cicd-images/GCP1-19.png)
2. Open de `Google Cloud Shell`
3. Voer het commando `gcloud app create` uit. Dit creeërt een appengine applicatie.
4. Ga in het hamburger menu naar `Cloud Build` -> `Settings`
![Cloud Build](https://storage.googleapis.com/jcore-cicd-images/GCP1-20.png)
5. Enable App Engine Admin 
6. Dubbel check of Service Account enabled is
![Cloud Build settings](https://storage.googleapis.com/jcore-cicd-images/GCP1-21.png)

### Trigger de build vanuit het project
Ga in het hamburger menu naar `Cloud Build` -> `Triggers`
![Cloud Build triggers](https://storage.googleapis.com/jcore-cicd-images/GCP1-22.png)

1. Voeg een trigger toe
![trigger config](https://storage.googleapis.com/jcore-cicd-images/GCP1-23.png)
2. Voeg aan de sourcecode een `cloudbuild.yaml` bestand toe in de folder `cloudbuild`
3. Voeg aan de sourcecode een `app.yaml` bestand toe in de root van het project.
4. Voeg aan de sourcecode een `Dockerfile` bestand toe in de root van het project.
5. Push de code naar de repository. In `Cloud Build` -> `History` zou je nu een build moeten zien.
6. Na enkele minuten is de app gedeployed in Appengine

cloudbuild.yaml:
```yaml
steps:
  - name: 'amazoncorretto:11'
    env: [ "GRADLE_USER_HOME=cache" ]
    entrypoint: "bash"
    args: [ "-c", "./gradlew build"]
  - name: 'gcr.io/cloud-builders/gcloud'
    args: ['app', 'deploy', 'app.yaml', '--no-promote']
    timeout: 1500s
timeout: 1800s
```
app.yaml:
```yaml
runtime: custom
env: flex
runtime_config:
  jdk: openjdk11
# Application Resources
resources:
  cpu: 2
  memory_gb: 2
  disk_size_gb: 10
  volumes:
  - name: ramdisk1
    volume_type: tmpfs
    size_gb: 0.5
# Automatic Scaling
automatic_scaling: 
  min_num_instances: 1 
  max_num_instances: 4 
  cool_down_period_sec: 180 
  cpu_utilization: 
    target_utilization: 0.6
```
Dockerfile:
```dockerfile
FROM amazoncorretto:11
COPY build/libs/gcpdemo-0.0.1-SNAPSHOT.jar ./app.jar
EXPOSE 8080
ENTRYPOINT java -jar app.jar
```

In App Enging of de build log kun je een url vinden naar de gedeployde app.